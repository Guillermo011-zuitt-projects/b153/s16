let a = 1;

while (a <= 5){
	console.log(a++);
}

// DO-while loop

let doWhileCounter = 1;
do {
	console.log(doWhileCounter);
} while (doWhileCounter === 0){
	console.log(doWhileCounter);
	doWhileCounter--;
}

let counter = 1;
do {
		console.log(counter);
		counter++;
} while (counter <= 19){
	console.log(counter++);
}

// For loop - more flexible
// for(initialization; expression/condition; finalExpression (++/--)){code block};

for(let count = 0; count <= 20; count++){
	console.log(count);
}

// array
let fruits = ["Apple", "Durian", "Kiwi", "Pineapple", "Strawberry"];
console.log(fruits[2]);

// Mini Activity
console.log(fruits[0]);
console.log(fruits[4]);

// arrayname.length - to to know the length of array
// arrayname.length-1 - to display the last item

for(let index = 0; index < fruits.length; index++){
	console.log(fruits[index]);}

// Mini Activity

let countries = ["Egypt", "USA", "Hongkong", "Canada", "Hawaii", "Philippines" ,"Libya"];

for (let index = 0; index < countries.length-1; index++){
	console.log(countries[index]);
}

// Array of Objects

let cars = [
{
	brand: "Toyota",
	type: "Sedan"
},
{
	brand: "Rolls Royce",
	type: "Luxury"
},
{
	brand: "Mazda",
	type: "Hatchback"
}

];

console.log(cars.length);// 3 items
console.log(cars[1]);//shows the second object

// Mini Activity
/*
create a loop that will print out te letters of the name individually and print out the number 3 instead when the letter to be printed out is vowel.
-How it works:
1. the loop will start at 0 for the value of 'i.'
2. It will check if 'i' is less than the length of MyName (e.g 0)
3. The if statement will check if the value of myName[i] converted to a lowercase letter is equivalent to any of the vowels. (e.g. myName[0] - a, myName[0]=e)
4. if the expression/condition i true, the console will print the letter
5. If the letter is not a vowel the console will print the letter
6. The value of 'i' will be incremented by 1
7. the loop will repeat from step 2 to 6 until the expression/condition of the loop is false
*/

let myName = "AdrIAn MadArang";
let vowel1 = "a";
let vowel2 = "e";
let vowel3 = "i";
let vowel4 = "o";
let vowel5 = "u";

for (let i = 0; i < myName.length; i++){
	if (myName[i].toLowerCase() == vowel1 || myName[i].toLowerCase() == vowel2 || myName[i].toLowerCase() == vowel3 || myName[i].toLowerCase() == vowel4 || myName[i].toLowerCase() == vowel5)
		console.log(3);
	else {
		console.log(myName[i]);
	}
}

/*
continue and break - the continue statement allows to the code to go to the enxt iteration of loop without finishing the exection of the following statemtn in code block
- skips the current loop and proceed to the next loop
*/

for (let a = 20; a > 0; a--){
	if (a%2 === 0){
		continue
}
	if (a < 10) {
		break
	}
	console.log(a);
}

let string = "Alejandro";

for (let i= 0; i < string.length; i++){
	if (string[i].toLowerCase() === "a")
		console.log("Continue to the next iteration.");
	continue
	if (string[i].toLowerCase() == "d"){
		break;
	}
}
